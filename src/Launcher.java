import java.util.ArrayList;

public class Launcher {

    private ArrayList<IRoute> listOfFeu;

    public void init() {

        listOfFeu = new ArrayList<>();

        ICommunicateFeuStrategy communicateFeuStrategy = new CommunicateFeu();
        ICommunicateServerStrategy communicateServerStrategy = new CommunicateServer();

        IRoute feu1 = new AutomaticFeu(1, 2, 1, 3, communicateServerStrategy, true);
        IRoute feu2 = new AutomaticFeu(2, 2, 1, 3, communicateServerStrategy, false);
        IRoute feu3 = new AutomaticFeu(3, 2, 1, 3, communicateServerStrategy, true);
        IRoute feu4 = new AutomaticFeu(4, 2, 1, 3, communicateServerStrategy, false);

        ICar car1 = new AutomaticCar(1, 1, communicateFeuStrategy);
        ICar car2 = new AutomaticCar(2, 2, communicateFeuStrategy);
        ICar car3 = new AutomaticCar(3, 1, communicateFeuStrategy);
        ICar car4 = new AutomaticCar(4, 1, communicateFeuStrategy);

        ICar car5 = new AutomaticCar(5, 1, communicateFeuStrategy);
        ICar car6 = new AutomaticCar(6, 2, communicateFeuStrategy);
        ICar car7 = new AutomaticCar(7, 1, communicateFeuStrategy);
        ICar car8 = new AutomaticCar(8, 1, communicateFeuStrategy);

        feu1.getListOfCar().add(car1);
        feu2.getListOfCar().add(car2);
        feu3.getListOfCar().add(car3);
        feu4.getListOfCar().add(car4);

        feu1.getListOfCar().add(car5);
        feu2.getListOfCar().add(car6);
        feu3.getListOfCar().add(car7);
        feu4.getListOfCar().add(car8);

        listOfFeu.add(feu1);
        listOfFeu.add(feu2);
        listOfFeu.add(feu3);
        listOfFeu.add(feu4);

        GestionFeuSystem gs = new GestionFeuSystem(listOfFeu);

        gs.start();


    }

    public static void main(String args[]) {
        Launcher launcher = new Launcher();
        launcher.init();
    }
}
