public class AutomaticCar extends Observateur implements ICar {
    private int direction;
    private ICommunicateFeuStrategy communicateFeuStrategy;

    private int id;

    public AutomaticCar(int id, int direction, ICommunicateFeuStrategy communicateFeuStrategy){
        this.direction = direction;
        this.id = id;

        this.communicateFeuStrategy = communicateFeuStrategy;

    }

    @Override
    public int getDirection(){
        return direction;
    }

    public void setDirection(int direction){
        this.direction = direction;
    }

    @Override
    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

}
