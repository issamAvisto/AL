import java.util.ArrayList;

abstract class AbstractFeu implements ISignal {

    protected int id;

    protected int time;

    protected boolean green;

    public AbstractFeu(int id, int time, boolean bool){
        this.id = id;
        this.time = time;
        this.green = bool;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getTime(){
        return getTime();
    }

    public void setTime(int time){
        this.time = time;
    }

    public boolean getGreen(){
        return green;
    }

    public void setGreen(boolean green){
        this.green = green;
    }

}
