import java.util.ArrayList;

public class AutomaticFeu extends AbstractFeu implements IObservable{
    private ArrayList<ICar> listOfCar;
    private ICommunicateServerStrategy communicate;

    private Thread thread;
    private volatile boolean running = false;


    private int min;

    private int max;

    public AutomaticFeu(int id, int time, int min, int max, ICommunicateServerStrategy communicate, boolean bool){
        super(id, time, bool);

        this.listOfCar = new ArrayList<>();

        this.min = min;

        this.max = max;

        this.communicate = communicate;
    }


    @Override
    public void run() {
            if (running) {
                throw new IllegalStateException("Animation is already running.");
            }

            // the reason we do not inherit from Runnable is that we do not want to
            // expose the void run() method to the outside world. We want to well
            // encapsulate the whole idea of a thread.
            // thread cannot be restarted so we need to always create a new one
            thread = new Thread() {
                @Override
                public void run() {
                    while (true) {
                        synchronized (this) {
                            while (!running) {
                                try {
                                    wait();
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }

                        try {
                            synchronized (this) {
                                Thread.sleep(1000);
                            }
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        simulate();
                    }
                }
            };

            // start the thread
            thread.start();
            // set the flag
            running = true;
    }

    public void simulate(){
        if(green){
            if(!listOfCar.isEmpty()) {
                System.out.println("Je suis le feu " + id + " et je laisse passer la voiture " + listOfCar.get(listOfCar.size() - 1).getId() + " car je suis vert");
                listOfCar.remove(listOfCar.size() - 1);
            }else{
                System.out.println("Je suis le feu "+id+", je suis vert mais personne ne veut passer par là");

            }
        }else{
            System.out.println("Je suis le feu "+id+" et je ne laisse passer personne car je suis rouge");
        }
    }
    public synchronized void stop() {
        if (!running) {
            throw new IllegalStateException("Animation is stopped.");
        }
        running = false;
    }

    public int getMin(){
        return min;
    }

    public void setMin(int min){
        this.min = min;
    }

    public int getMax(){
        return max;
    }

    public void setMax(int max){
        this.max = max;
    }

    @Override
    public ArrayList<ICar> getListOfCar() {
        return listOfCar;
    }

    @Override
    public void changement() {
        this.green = !this.green;
    }
}
