import java.lang.reflect.Type;

abstract class Factory {

    public IRoute formerRoute(Type type){
        IRoute route = this.creerRoute(type);
        return route;
    }

    public abstract IRoute creerRoute(Type type);
}
