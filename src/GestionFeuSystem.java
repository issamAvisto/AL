import java.util.ArrayList;

public class GestionFeuSystem {

    private ArrayList<IRoute> listOfFeu;
    private Thread thread;
    private volatile boolean running = false;

    public GestionFeuSystem(ArrayList<IRoute> listOfFeu){
        this.listOfFeu = listOfFeu;
    }

    public void start(){
        for(IRoute r : listOfFeu){
            r.run();
        }

        if (running) {
            throw new IllegalStateException("Animation is already running.");
        }

        // the reason we do not inherit from Runnable is that we do not want to
        // expose the void run() method to the outside world. We want to well
        // encapsulate the whole idea of a thread.
        // thread cannot be restarted so we need to always create a new one
        thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    synchronized (this) {
                        while (!running) {
                            try {
                                wait();
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }

                    try {
                        synchronized (this) {
                            Thread.sleep(10000);
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    simulate();
                }
            }
        };

        // start the thread
        thread.start();
        // set the flag
        running = true;
    }

    private void simulate(){
        System.out.println("Je suis le système de gestion et je décide que chaque feu doit changer d'état");
        for(IRoute r : listOfFeu){
           r.changement();
        }
    }

}
